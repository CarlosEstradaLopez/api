//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

var bodyparser =require('body-parser');
app.use(bodyparser.json())

app.listen(port);



console.log('todo list RESTful API server started on: ' + port);

var MovimientosJSON = require('./movimientos_V2.json');

app.get("/",function (req,res) {
  //res.send("Hola Mundo NodeJS");
  res.sendfile(path.join(__dirname,"index.html"));
});

app.get("/Clientes",function (req,res) {
  res.send("Aquí Están los clientes devueltos");
});

app.get("/Clientes/:idCliente",function (req,res) {
  res.send("Aquí tiene al cliente numero:"+req.params.idCliente);
});

app.get("/Movimientos/v1",function (req,res) {
  res.sendfile("movimientos_V1.json");
});

app.get("/Movimientos/v2/:index/:otroparametro",function (req,res) {
  console.log(req.params.index)
  console.log(req.params.otroparametro)
  console.log(MovimientosJSON.length)
  res.send("Todo OK")
  //res.send(MovimientosJSON[req.params.index]);
});

app.get("/Movimientosq/v2",function (req,res) {
  console.log(req.query)
  res.send("recibido");
});

app.get("/Movimientos/v2",function (req,res) {
  res.send(MovimientosJSON);
});

app.post("/", function (req,res) {
  res.send("Hemos recibido su peticion POST");
});

app.post("/Movimientos/v2", function (req,res) {
  var nuevo=req.body
  nuevo.id=MovimientosJSON.length +1;
  MovimientosJSON.push(nuevo)
  res.send("Movimiento dado de alta");
});
